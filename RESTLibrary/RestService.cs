﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RESTLibrary
{
    public static class RestService
    {
        static readonly string _contentType = "application/json";

        public static async Task<JObject> SendRequest(string uri, JObject context)
        {
            try
            {
                using (var client = new HttpClient()

                {
                    Timeout = TimeSpan.FromMinutes(10)
                })
                {

                    var message = new HttpRequestMessage(HttpMethod.Post, uri);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_contentType));

                    message.Content = new StringContent(JsonConvert.SerializeObject(context), Encoding.UTF8, _contentType);

                    try
                    {
                        var response = await client.SendAsync(message);

                        var responseBody = await response.Content.ReadAsStringAsync();

                        return JObject.Parse(responseBody);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
        }
    }
}
