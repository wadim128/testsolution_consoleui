﻿using Newtonsoft.Json.Linq;
using RESTLibrary;
using System;
using System.Threading.Tasks;

namespace ConsoleUI
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var loginServiceUrl = @"http://localhost:21943/LoginService.svc/login";
            
            Console.Write("Введите логин: ");
            var login = Console.ReadLine();
            Console.Write("Введите пароль: ");
            var password = Console.ReadLine();

            var response = await RestService.SendRequest(loginServiceUrl, 
                new JObject { { "login", login }, { "password", Cryptography.Cryptography.ComputeSHA256(password) } });

            if (response == null)
            {
                Console.WriteLine("Произошла непредвиденная ошибка, повторите позднее");
            }
            else
            {
                Console.WriteLine(response["LoginResult"].ToString());
            }
            Console.ReadLine();
        }
    }
}
